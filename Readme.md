Wallpaper Changer
=================
About
-----
Wallpaper Changer is a little tool written in C# to change the Wallpaper on a Windows system by changing the value in the Registry instead of doing it "the correct way".
The reason for doing it this way is e.g. to change the Wallpaper on a restricted system (like Windows 7 Starter or on a system with enforced GPOs), where changing the wallpaper isn't possible by doing it the normal way.
Please note that:
* This tool isn't coded that well (I created it as an exercise), so don't please expect high quality code. :)
* Bypassing GPO restrictions may get you in trouble, so USE IT ON YOUR OWN RISK!
* A compiled ready-to-use binary (.NET Framework 4 required) is available under the "Compiled Binary" directory.
* This program doesn't check if the entered path is valid, so entering an invalid path and applying the changes may lead Windows to use a solid-colored (grey) fallback "wallpaper".
* The program tries to store the new Wallpaper in User\AppData\Roaming\Wallpaper Changer. If for some reason this doesn't work, it will fallback to use the provided path and instead write this one to the Registry. If that's the case, make sure you don't move or delete the wallpaper from there, otherwise it might (or will) break on the next logon.

Usage
-----
To change the wallpaper:

1. Enter the path (or use the 'Browse' dialog) at which the Wallpaper is located.
2. Press "Write to Registry and Reload"
3. Additionally, if the wallpaper hasn't been changed even though you pressed the first button, use the "Spam Reload" button a couple of times (or raise the counter next to it) and see if it changes. If it doesn't, that's strange.

To save the current wallpaper, press the "Save current Wallpaper" button. The program will try to save it in the working directory and will show you the exact path in a Messagebox.

License information
-------------------
Use this tool as you like, but please note that the program icon was taken from [IconArchive](http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Apps-preferences-desktop-wallpaper-icon.html), created by the Oxygen Team and release under the [GPL](https://www.gnu.org/copyleft/lesser.html) license, so you may need to remove the icon if you're not okay with the license.