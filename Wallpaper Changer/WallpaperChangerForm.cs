﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Wallpaper_Changer
{
    public partial class uiForm : Form
    {
        Boolean fallbackToCurrentDir = false;
        String appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)  + "\\Wallpaper Changer";
        public uiForm()
        {
            InitializeComponent();
        }

        private void btPath_Click(object sender, EventArgs e)
        {
            openFileDialogViabtBrowse.ShowDialog();
        }

        private void txbxPath_TextChanged(object sender, EventArgs e)
        {
            btSetWallpaper.Enabled = true;
        }

        private void openFileDialogbtBrowse_FileOk(object sender, CancelEventArgs e)
        {
            txbxPath.Text = openFileDialogViabtBrowse.FileName;
        }

        private void btSetWallpaper_Click(object sender, EventArgs e)
        {
            // There is no sanity check here, so entering a wrong path will result in a fallback (grey?) wallpaper.
            if (!System.IO.Directory.Exists(appDataPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(appDataPath);
                    // copy the file and check the result to see if we need to fallback.
                }
                catch
                {
                    MessageBox.Show("Wallpaper Changer couldn't create a folder for storing the wallpapers, so Wallpaper Changer is using your provided path. Please make sure you don't move or delete your wallpaper from this location!");
                    fallbackToCurrentDir = true;
                }
            }
            if (!fallbackToCurrentDir)
            {
                if (!this.copyCurrentWallpaperToAppDataAndApply(txbxPath.Text))
                {
                    fallbackToCurrentDir = true;
                }

            }
            // dual if check, because the first if check is able to change the state of fallbackToCurrentDir
            if (fallbackToCurrentDir)
            {
                Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "Wallpaper", txbxPath.Text);
            }
            ReloadDesktopValues();
            btSetWallpaper.Enabled = false;
        }

        public void ReloadDesktopValues()
        {
            System.Diagnostics.Process.Start("RUNDLL32.EXE", "user32.dll,UpdatePerUserSystemParameters");
        }


        private void btReloadDesktopValues_Click(object sender, EventArgs e)
        {
            int cycles = 0;
            while(cycles <= nudReloadCount.Value)
            {
                cycles = cycles + 1;
                ReloadDesktopValues();
            }
        }

        private void btSaveCurrentWallpaper_Click(object sender, EventArgs e)
        {
            // null is the returned string we get if the value "Wallpaper" isn't found (which shouldn't be the case)
            String currentWallpaper = (String) Registry.GetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "Wallpaper", null);
            if (currentWallpaper != null)
            {
                try
                {
                    // Create a name with a date to store more than one wallpaper and avoid overwriting existing ones.
                    String savedFilename = "SavedWallpaper" + DateTime.Now.ToString("(dd_MM_yyyy-hh_mm_ss)") + currentWallpaper.Substring(currentWallpaper.Length - 4);
                    MessageBox.Show(savedFilename);
                    System.IO.File.Copy(currentWallpaper, savedFilename);
                    MessageBox.Show("Your wallpaper has been saved under the following location:" + Environment.NewLine + System.IO.Directory.GetCurrentDirectory() + "\\" + savedFilename);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("The following error occured while trying to save your wallpaper:" + Environment.NewLine + ex);
                }
            }
        }

        private void uiForm_Load(object sender, EventArgs e)
        {
            btSetWallpaper.Enabled = false;
        }

        private Boolean copyCurrentWallpaperToAppDataAndApply(String path)
        {
            if (!fallbackToCurrentDir)
            {
                try
                {
                    System.IO.File.Copy(path, appDataPath + "\\Wallpaper" + path.Substring(path.Length - 4), true);
                    Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "Wallpaper", appDataPath + "\\Wallpaper" + path.Substring(path.Length - 4));
                    // return true for success.
                    return true;
                }
                catch(Exception ex)
                {
                    MessageBox.Show("An error occured while trying to copy the file, so Wallpaper Changer is using your provided path. Please make sure you don't move or delete your wallpaper from this location!" + Environment.NewLine + ex);
                    // return false for failure.
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
