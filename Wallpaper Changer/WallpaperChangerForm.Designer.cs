﻿namespace Wallpaper_Changer
{
    partial class uiForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uiForm));
            this.openFileDialogViabtBrowse = new System.Windows.Forms.OpenFileDialog();
            this.btPath = new System.Windows.Forms.Button();
            this.txbxPath = new System.Windows.Forms.TextBox();
            this.grpboxPfad = new System.Windows.Forms.GroupBox();
            this.btSetWallpaper = new System.Windows.Forms.Button();
            this.btReloadDesktopValues = new System.Windows.Forms.Button();
            this.lbReloadNote = new System.Windows.Forms.Label();
            this.nudReloadCount = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btSaveCurrentWallpaper = new System.Windows.Forms.Button();
            this.grpboxPfad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReloadCount)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialogViabtBrowse
            // 
            this.openFileDialogViabtBrowse.Filter = "Pictures|*.png;*.jpg;*.bmp";
            this.openFileDialogViabtBrowse.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialogbtBrowse_FileOk);
            // 
            // btPath
            // 
            this.btPath.Location = new System.Drawing.Point(500, 19);
            this.btPath.Name = "btPath";
            this.btPath.Size = new System.Drawing.Size(90, 23);
            this.btPath.TabIndex = 0;
            this.btPath.Text = "Browse";
            this.btPath.UseVisualStyleBackColor = true;
            this.btPath.Click += new System.EventHandler(this.btPath_Click);
            // 
            // txbxPath
            // 
            this.txbxPath.Location = new System.Drawing.Point(6, 19);
            this.txbxPath.Name = "txbxPath";
            this.txbxPath.Size = new System.Drawing.Size(470, 20);
            this.txbxPath.TabIndex = 1;
            this.txbxPath.WordWrap = false;
            this.txbxPath.TextChanged += new System.EventHandler(this.txbxPath_TextChanged);
            // 
            // grpboxPfad
            // 
            this.grpboxPfad.Controls.Add(this.txbxPath);
            this.grpboxPfad.Controls.Add(this.btPath);
            this.grpboxPfad.Location = new System.Drawing.Point(12, 12);
            this.grpboxPfad.Name = "grpboxPfad";
            this.grpboxPfad.Size = new System.Drawing.Size(596, 65);
            this.grpboxPfad.TabIndex = 2;
            this.grpboxPfad.TabStop = false;
            this.grpboxPfad.Text = "Path";
            // 
            // btSetWallpaper
            // 
            this.btSetWallpaper.Location = new System.Drawing.Point(76, 83);
            this.btSetWallpaper.Name = "btSetWallpaper";
            this.btSetWallpaper.Size = new System.Drawing.Size(392, 80);
            this.btSetWallpaper.TabIndex = 3;
            this.btSetWallpaper.Text = "Write to Registry and Reload";
            this.btSetWallpaper.UseVisualStyleBackColor = true;
            this.btSetWallpaper.Click += new System.EventHandler(this.btSetWallpaper_Click);
            // 
            // btReloadDesktopValues
            // 
            this.btReloadDesktopValues.Location = new System.Drawing.Point(76, 195);
            this.btReloadDesktopValues.Name = "btReloadDesktopValues";
            this.btReloadDesktopValues.Size = new System.Drawing.Size(392, 80);
            this.btReloadDesktopValues.TabIndex = 5;
            this.btReloadDesktopValues.Text = "Spam Reload";
            this.btReloadDesktopValues.UseVisualStyleBackColor = true;
            this.btReloadDesktopValues.Click += new System.EventHandler(this.btReloadDesktopValues_Click);
            // 
            // lbReloadNote
            // 
            this.lbReloadNote.AutoSize = true;
            this.lbReloadNote.Location = new System.Drawing.Point(80, 278);
            this.lbReloadNote.Name = "lbReloadNote";
            this.lbReloadNote.Size = new System.Drawing.Size(388, 13);
            this.lbReloadNote.TabIndex = 6;
            this.lbReloadNote.Text = "Note: Sometimes it is necessary to reload multiply times to see the new wallpaper" +
    ".";
            // 
            // nudReloadCount
            // 
            this.nudReloadCount.Location = new System.Drawing.Point(522, 227);
            this.nudReloadCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudReloadCount.Name = "nudReloadCount";
            this.nudReloadCount.Size = new System.Drawing.Size(45, 20);
            this.nudReloadCount.TabIndex = 7;
            this.nudReloadCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(509, 211);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Reload Count";
            // 
            // btSaveCurrentWallpaper
            // 
            this.btSaveCurrentWallpaper.Location = new System.Drawing.Point(76, 315);
            this.btSaveCurrentWallpaper.Name = "btSaveCurrentWallpaper";
            this.btSaveCurrentWallpaper.Size = new System.Drawing.Size(392, 80);
            this.btSaveCurrentWallpaper.TabIndex = 9;
            this.btSaveCurrentWallpaper.Text = "Save current Wallpaper";
            this.btSaveCurrentWallpaper.UseVisualStyleBackColor = true;
            this.btSaveCurrentWallpaper.Click += new System.EventHandler(this.btSaveCurrentWallpaper_Click);
            // 
            // uiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(634, 421);
            this.Controls.Add(this.btSaveCurrentWallpaper);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudReloadCount);
            this.Controls.Add(this.lbReloadNote);
            this.Controls.Add(this.btReloadDesktopValues);
            this.Controls.Add(this.btSetWallpaper);
            this.Controls.Add(this.grpboxPfad);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "uiForm";
            this.Text = "Wallpaper Changer";
            this.Load += new System.EventHandler(this.uiForm_Load);
            this.grpboxPfad.ResumeLayout(false);
            this.grpboxPfad.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReloadCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialogViabtBrowse;
        private System.Windows.Forms.Button btPath;
        private System.Windows.Forms.TextBox txbxPath;
        private System.Windows.Forms.GroupBox grpboxPfad;
        private System.Windows.Forms.Button btSetWallpaper;
        private System.Windows.Forms.Button btReloadDesktopValues;
        private System.Windows.Forms.Label lbReloadNote;
        private System.Windows.Forms.NumericUpDown nudReloadCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btSaveCurrentWallpaper;
    }
}

